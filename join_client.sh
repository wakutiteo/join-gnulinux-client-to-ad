#!/bin/bash
#
# Join Ubuntu client to Active Directory

# Colors
WARNINGCOLOR="\033[1;91m"
SUCCESSCOLOR="\033[1;92m"
INFOCOLOR="\e[1;34m"
NORMALCOLOR="\033[0m"

# Error function
exit_error() {
	echo -e "\n${WARNINGCOLOR}$1${NORMALCOLOR}"
	exit 1
}
# Info function
info_msg() {
	echo -e "\n${INFOCOLOR}$1${NORMALCOLOR}"
}

# Server name
server_name="server1"

# Domain
domain="markel.com"
DOMAIN=$(tr "[:lower:]" "[:upper:]" <<< $domain) # Do not modify this one
admin_user="administrator"

# Client hostname
hostname="hostname"

# Network config
# If you have problems try to comment the line below ($connection_name variable) and
# uncomment the following (replace the name of the network connection with the name of your connection)
connection_name=$(nmcli -f name connection show --active | head -n 2 | tail -n 1 | xargs) # Get the network active connection name
#connection_name="Conexión cableada 1"

dns1="192.168.1.52"
dns2="208.67.222.222"

# Change this to yes when the VARIABLES ADOVE are properly configured
configured="no"


#### THE SCRIPT START HERE ####

# If the user isn't superuser
if [[ $(id -u) -ne 0 ]]; then
	exit_error "MUST BE EXECUTED AS ROOT :)"
	exit 1
fi

# If user have changed the variables
if [[ $configured = "yes" ]]; then
	echo CONTINUING...
else
	exit_error "To run this script you must have configure it," \
		"open it with any text editor enter your values to the variables"
	exit 1
fi


# Configure DNS
info_msg "Configuring DNS..."
nmcli connection modify "$connection_name" ipv4.ignore-auto-dns yes
nmcli connection modify "$connection_name" ipv4.dns "$dns1 $dns2"

nmcli connection down "$connection_name"
nmcli connection up "$connection_name"

# Set hostname
info_msg "Configuring hostname..."
hostnamectl set-hostname $hostname
ping -c3 $domain || exit_error "There is no conection with the domain"

# Sync time via NTP with server
info_msg "Configuring NTP with AD server..."
sed -ie "s/^.\(NTP=\).*/\1$server_name.$domain/" /etc/systemd/timesyncd.conf # Important double quotes fot variables
timedatectl set-ntp true
systemctl restart systemd-timesyncd
timedatectl --adjust-system-clock

# Installing packages non-interactivelly
info_msg "Installing necesary packages..."
export DEBIAN_FRONTEND=noninteractive # also works on Ubuntu
apt install -y samba krb5-config krb5-user winbind libpam-winbind libnss-winbind

# Config /etc/krb5.conf
info_msg "Configuring Kerberos..."
sed -ie "s/\(default_realm = \).*/\1$DOMAIN/" /etc/krb5.conf
sed -ie "/^\[realms\]$/a\ \t$DOMAIN = {\n\t\tkdc = $server_name.$domain\n\t\tadmin_server = $server_name.$domain\n\t}" /etc/krb5.conf

# Config Samba4 AD DC
# Don't overwrite if exists
info_msg "Configuring Samba..."
cp -n /etc/samba/smb.conf /etc/samba/smb.conf.backup
echo "
[global]
	workgroup = $(echo $DOMAIN | cut -d '.' -f 1)
	realm = $DOMAIN
	netbios name = UBUNTU18
	security = ADS
	dns forwarder = 192.168.1.1 # CAMBIAR

	idmap config * : backend = tdb
	idmap config *:range = 50000-1000000

	template homedir = /home/%D/%U
	template shell = /bin/bash
	winbind use default domain = true
	winbind offline logon = false
	winbind nss info = rfc2307
	winbind enum users = yes
	winbind enum groups = yes

	vfs objects = acl_xattr
	map acl inherit = Yes
	store dos attributes = Yes" > /etc/samba/smb.conf # Be careful with this redirection

# Restart services for configuration changes to take effect
info_msg "Restarting services..."
systemctl restart smbd nmbd winbind # winbind error, i don't know why. But after joining to AD you can restart service
systemctl stop samba-ad-dc
systemctl enable smbd nmbd winbind

# Join client to Samba4 AD DC
info_msg "Enter a password to join Active Directory..."
net ads join -U $admin_user || exit_error "Probably the time & date it's not synchronized"

# Config /etc/nsswitch.conf
info_msg "Configuring NSS..."
sed -ie 's/^\(passwd:\).*/\1\t\tcompat winbind/' /etc/nsswitch.conf
sed -ie 's/^\(group:\).*/\1\t\tcompat winbind/' /etc/nsswitch.conf
sed -ie 's/^\(shadow:\).*/\1\t\tcompat winbind/' /etc/nsswitch.conf
sed -ie 's/^\(hosts:\).*/\1\t\tfiles dns/' /etc/nsswitch.conf

# Config /etc/pam.d/common-password
info_msg "Cofiguring PAM..."
sed -ie 's/^\(password.*\)use_authtok /\1/' /etc/pam.d/common-password

# Config /etc/pam.d/common-account | Execute only if line doesn't exists
grep -q "session.*required.*pam_mkhomedir.so" /etc/pam.d/common-account || \
	echo "session    required    pam_mkhomedir.so    skel=/etc/skel/    umask=0022	silent" >> /etc/pam.d/common-account

info_msg "Restarting services..."
systemctl restart smbd nmbd winbind # Again why?

# Give to administrator sudo privileges
info_msg "Giving to AD administrator user sudo privileges..."
usermod -aG sudo $admin_user  || exit_error "Something went wrong at the client join"

# Reboot system
if [[ $? -eq 0 ]]; then
	echo -e "\n${SUCCESSCOLOR}Reboot your system for changes to effect${NORMALCOLOR}\n"
fi

